from collections import defaultdict, namedtuple, Counter
import itertools
import re

class crossword_solver:

    def __init__(self, puzzle: str=None, words: str=None):
        if puzzle:
            self.puzzle, self.words = puzzle.split(" "), words.split(";")
        else:
            self.read_empty_crossword()


    def read_empty_crossword(self)-> str:

        print('Enter crossword puzzle. Each row should be seperated by a space.')
        puzzle = input()
        print('Enter words to fill in the crossword puzzle. Each word should be seperated ";".')
        words = input()
        self.puzzle, self.words = puzzle.split(" "), words.split(';')


    def find_all_possible_combinations(self):

        self.word_lengths = list(map(len,self.words))
        # Mapping length to positions
        self.map_length_pos = defaultdict(list)
        self.find_hypens_in_the_puzzle(orientation='row')
        self.find_hypens_in_the_puzzle(orientation='column')

        possible_positions_for_each_word = list(map(lambda x:self.map_length_pos[x], self.word_lengths))

        # Getting rid of impossible combination.
        self.possible_combinations = []
        for combination in itertools.product(*possible_positions_for_each_word):
            if Counter(combination).most_common(1)[0][1] == 1:
                self.possible_combinations.append(combination)


    def fit_words_to_puzzle(self):

        for comb in self.possible_combinations:
            puzzle_list = [[*row] for row in self.puzzle]
            try:
                for pos,word in zip(comb,self.words):
                    puzzle_list = self.fill(puzzle_list, pos, word)

            except:
                pass
             # if the try block is executed properly, that means the words are
             # fitted properly in the crossword. The else statement will return the filled puzzle
            else:
                return ["".join(row) for row in puzzle_list]


    def find_hypens_in_the_puzzle(self, orientation='row'):

        if orientation=='row':
            position, puzzle = namedtuple('row_wise',['start_col','end_col','row']), self.puzzle
        else:
            # Find the transporse of puzzle
            position, puzzle = namedtuple('col_wise',['start_row','end_row','column']), list(map(lambda x: "".join(x),zip(*self.puzzle)))

        for indx, string in enumerate(puzzle):
            matches = list(re.finditer('-{1,}',string))

            for match_ in matches:
                pos, lenght = position(match_.span()[0],match_.span()[1],indx), len(match_.group())
                if lenght in self.word_lengths:
                    self.map_length_pos[lenght].append(pos)


    def fill(self, puzzle, pos, word):

        if pos.__class__.__name__ == 'col_wise':
            for indx,i in enumerate(range(pos.start_row, pos.end_row)):
                if puzzle[i][pos.column]=='-':
                    puzzle[i][pos.column]=word[indx]
                elif puzzle[i][pos.column]==word[indx]:
                    pass
                else:
                    raise Exception("Not the right combination")
        else:
            for indx,i in enumerate(range(pos.start_col, pos.end_col)):
                if puzzle[pos.row][i]=='-':
                    puzzle[pos.row][i]=word[indx]
                elif puzzle[pos.row][i]==word[indx]:
                    pass
                else:
                    raise Exception("not the right combination")

        return puzzle


    def solve(self):
        self.find_all_possible_combinations()
        solved_puzzle = self.fit_words_to_puzzle()
        for i in solved_puzzle:
            print(i)

if __name__ == '__main__':
    g = crossword_solver()
    g.solve()
