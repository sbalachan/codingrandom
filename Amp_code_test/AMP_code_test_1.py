from collections import defaultdict, namedtuple
import itertools
from collections import Counter
import re

def read_empty_crossword()-> str:
    '''
    This Function reads the puzzel and the words.
    words must be a string separated by ';' eg: AGRA;NORWAY;ENGLAND;GWALIOR
    Puzzel must be entered a string separated by space.
    eg: '+-++++++++ +-++++++++ +-------++'
    @return: returns 2 lists. puzzel is a list of rows where each row is string
             words is list of words
    @rtype: list

    '''

    print('enter crossword puzzel. Each row should be seperated by a space.')
    puzzel = input()
    print('enter words to fill in the crossword puzzel. Each word should be seperated ";".')
    words = input()
    return puzzel.split(" "), words.split(';')


def find_all_possible_combinations(puzzel, words):
    '''
    find all possible combinations to fit words in the puzzel based on length alone.
    @param puzzel: list of strings
    @type puzzel: list
    @param words: list of strings
    @type words: list
    @return: possible combinations to fit words in the puzzel based on length alone.
             list of namedtuples. eg: [(col_wise(start_row=5, end_row=9, column=5),
                                        row_wise(start_col=1, end_col=7, row=5),
                                        col_wise(start_row=0, end_row=7, column=1)]
    @rtype: list
    '''
    word_lengths = list(map(len,words))

    # Mapping length to positions
    map_length_pos = defaultdict(list)

    # Make positions classes
    col_wise_pos = namedtuple('col_wise',['start_row','end_row','column'])
    row_wise_pos = namedtuple('row_wise',['start_col','end_col','row'])

    # Find row wise positions
    for row, string in enumerate(puzzel):
        matches = list(re.finditer('-{1,}',string))
        for match_ in matches:
            pos, lenght = row_wise_pos(match_.span()[0],match_.span()[1],row), len(match_.group())
            if lenght in word_lengths:
                map_length_pos[lenght].append(pos)

    # Find the transporse of puzzle
    puzzle_t = list(map(lambda x: "".join(x),zip(*puzzel)))

    # Find column wise positions
    for col, string in enumerate(puzzle_t):
        matches = list(re.finditer('-{1,}',string))
        for match_ in matches:
            pos, lenght = col_wise_pos(match_.span()[0],match_.span()[1],col), len(match_.group())
            if lenght in word_lengths:
                map_length_pos[lenght].append(pos)

    possible_positions_for_each_word = list(map(lambda x:map_length_pos[x], word_lengths))

    # Getting rid of impossible combination
    combinations = []
    for combination in itertools.product(*possible_positions_for_each_word):
        if Counter(combination).most_common(1)[0][1] == 1:
            combinations.append(combination)

    return combinations



def fill(puzzel, pos, word):
    '''
    This functions attempts to fill the crossword puzzle based on a combination of words
    and returns the fitted puzzle if the combination fits. Raises and exception if the
    combination of words does not fit the puzzle.
    @param puzzle: puzzle represented by list of strings
    @type puzzle: list
    @param words: list of strings
    @type words: list
    @param pos: one specific combination of words amon all possible combinations
    @type pos: list
    @return: returns a list of list of characters that represents a filled crossword puzzle
    @rtype: list

    '''

    if pos.__class__.__name__ == 'col_wise':
        for indx,i in enumerate(range(pos.start_row, pos.end_row)):
            if puzzel[i][pos.column]=='-':
                puzzel[i][pos.column]=word[indx]
            elif puzzel[i][pos.column]==word[indx]:
                pass
            else:
                raise Exception("no the right combination")

    else:
        for indx,i in enumerate(range(pos.start_col, pos.end_col)):
            if puzzel[pos.row][i]=='-':
                puzzel[pos.row][i]=word[indx]
            elif puzzel[pos.row][i]==word[indx]:
                pass
            else:
                raise Exception("not the right combination")

    return puzzel

def fit_words_to_puzzel(possible_combinations, puzzel, words):
        '''
        This function tries to fit words in to the puzzle and finds the right way to do it.
        @param possible_combinations: possible combinations to fit words in the puzzel based on length alone.
                                      list of namedtuples. eg: [(col_wise(start_row=5, end_row=9, column=5),
                                                                row_wise(start_col=1, end_col=7, row=5),
                                                                col_wise(start_row=0, end_row=7, column=1)]
        @type possible_combinations: list
        @param puzzle: puzzle represented by list of strings
        @type puzzle: list
        @param words: list of strings
        @type words: list
        @return: returns a list of string that represents a filled crossword puzzle
        @rtype: list

        '''

        for comb in possible_combinations:

            puzzel_list = [[*row] for row in puzzel]

            try:
                for pos,word in zip(comb,words):
                    puzzel_list = fill(puzzel_list, pos, word)

            except:
                pass
             # if the try block is executed properly, that means the words are
             # fitted properly in the crossword. The else statement will return the filled puzzel
            else:
                return ["".join(row) for row in puzzel_list]

if __name__ == '__main__':

    puzzel, words = read_empty_crossword()
    all_possible_combinations = find_all_possible_combinations(puzzel,words)
    solved_puzzel = fit_words_to_puzzel(all_possible_combinations, puzzel, words)
    for i in solved_puzzel:
        print(i)


'''
Sample input 1:
Puzzle:
+-++++++++ +-++++++++ +-------++ +-++++++++ +-++++++++ +------+++ +-+++-++++ +++++-++++ +++++-++++ ++++++++++
Words:
AGRA;NORWAY;ENGLAND;GWALIOR


Output:
['+E++++++++',
 '+N++++++++',
 '+GWALIOR++',
 '+L++++++++',
 '+A++++++++',
 '+NORWAY+++',
 '+D+++G++++',
 '+++++R++++',
 '+++++A++++',
 '++++++++++']


Sample input 2:
Puzzle:
+-++++++++ +-++++++++ +-++++++++ +-----++++ +-+++-++++ +-+++-++++ +++++-++++ ++------++ +++++-++++ +++++-++++
Words:
LONDON;DELHI;ICELAND;ANKARA


Output:
['+L++++++++',
 '+O++++++++',
 '+N++++++++',
 '+DELHI++++',
 '+O+++C++++',
 '+N+++E++++',
 '+++++L++++',
 '++ANKARA++',
 '+++++N++++',
 '+++++D++++']

'''
