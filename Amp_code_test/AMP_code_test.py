import math
import os
import random
import re
import sys
# Additional functions
from itertools import zip_longest
from typing import List



def read_matrix()-> List[str]:
    '''
    This function allows users to set the dimension of the encoded message
    and then enter the message.
    @return: returns a list of strings.
    @rtype: List
    '''

    print(f'Please enter the number of rows and column seperated by a space')
    first_multiple_input = input().rstrip().split()
    n = int(first_multiple_input[0]) # Rows
    m = int(first_multiple_input[1]) # Columns)

    assert 0<n<100 and 0<m<100, "Number of rows and columns must be between 0 and 100"

    print(f'Please enter the elements of matrix')

    matrix = []
    for _ in range(n):
        matrix_item = input()
        matrix.append(matrix_item)

    return matrix

def decode_matrix(matrix:List[str])-> str:
    '''
    This function accepts a list of string as input and returns the decoded.
    message as a string.
    @param matrix: list of strings. (eg. ['abc', ')9m'])
    @type matrix: List[str]
    @return: string
    @rtype: str
    '''

    # A new matrix is created. Each element of the new matix is a character
    matrix_of_characters = list(map(lambda x: [*x], matrix))
    # matrix_of_characters is transporsed below
    matrix_transporsed = list(zip_longest(*matrix_of_characters, fillvalue=" "))
    res1 = ''.join(map(lambda x: "".join(x), matrix_transporsed))
    unformatted_string = "".join(res1)
    # replaces the symbols or spaces between two alphanumeric characters
    # with a single space ' ' for better readability.
    pattern_symbols_between_words = re.compile(r'(?<=\w)(\W+)(?=\w)') #identify the symbols between words.

    return re.sub(pattern_symbols_between_words, " ",unformatted_string) # Replace symbols between words.


if __name__ == '__main__':
    matrix = read_matrix()
    decoded_message = decode_matrix(matrix)
    print(f'The decoded message is: {decoded_message}')
